<?php
// add page template
function wpse_288589_add_template_to_select( $post_templates, $wp_theme, $post, $post_type ) {

    // Add custom template named template-custom.php to select dropdown
    $post_templates['download-page.php'] = __('Page Download');

    return $post_templates;
}

add_filter( 'theme_page_templates', 'wpse_288589_add_template_to_select', 10, 4 );


/**
 * Check if current page has our custom template. Try to load
 * template from theme directory and if not exist load it
 * from root plugin directory.
 */
function wpse_288589_load_plugin_template( $template ) {

    if(  get_page_template_slug() === 'download-page.php' ) {

        if ( $theme_file = locate_template( array( 'download-page.php' ) ) ) {
            $template = $theme_file;
        } else {
            $template = plugin_dir_path( __FILE__ ) . 'download-page.php';
        }
    }

    if($template == '') {
        throw new \Exception('No template found');
    }

    return $template;
}
add_filter( 'template_include', 'wpse_288589_load_plugin_template' );