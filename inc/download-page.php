<?php

if (isset($_REQUEST['_wpnonce']) && wp_verify_nonce($_REQUEST['_wpnonce'], 'get_link_qa')) :

    if (empty($_POST['id_post'])) :
        $noti = "Not found APP";
    else:
        $data_file = get_field('file_origin_upload', $_POST['id_post']);
        if (empty($data_file['url'])) :
            $noti = "Not found link download";
        else:
        $mime_type = $data_file['mime_type'];
        $filename = $data_file['filename'];
        $file_download= $data_file['url'];
            header("Content-Description: File Transfer");
            header("Content-type: {$mime_type}");
            header("Content-Disposition: attachment; filename=\"{$filename}\"");
            header("Content-Length: " . filesize($file_download));
            header('Pragma: public');
            header("Expires: 0");
            readfile($file_download);
        endif;
    endif;

else:
    $noti =  "Invalid access";

endif;
echo $noti;

